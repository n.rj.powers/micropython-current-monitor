#Built for MicroPython
import time
from machine import I2C, Pin
i2c = I2C(scl = Pin(5), sda = Pin(4), freq=100000)

import ssd1306
display = ssd1306.SSD1306_I2C(128, 32, i2c)

import ina219
sensor = ina219.INA219(i2c)

#import wifi
#wifi.wifi_connect()

import ujson
from umqtt.simple import MQTTClient
server="192.168.0.35"

global a_button
global b_button
global c_button
a_button = machine.Pin(0, machine.Pin.IN, machine.Pin.PULL_UP)
b_button = machine.Pin(16, machine.Pin.IN)
c_button = machine.Pin(2, machine.Pin.IN, machine.Pin.PULL_UP)

selection = 0

#Define helper functions for CircuitPython
def clear():
	print("\x1B\x5B2J", end="")
	print("\x1B\x5BH", end="")

def mqtt_send(server="192.168.0.35", current_avg=0, time_avg=0, current_once=0, voltage_once=0, counter=0):
	c = MQTTClient("meter_monitor", server)
	c.connect()
	c.publish(b"current_avg", "{:.2f}".format(current_avg))
	c.publish(b"time_avg", "{:.2f}".format(time_avg))
	c.publish(b"current_once", "{:.2f}".format(current_once))
	c.publish(b"voltage_once", "{:.2f}".format(voltage_once))
	c.publish(b"counter", "{:.0f}".format(counter))
	c.disconnect()
	
def up_arrow(pos_x, pos_y, disp):
	display.hline(pos_x+3,pos_y+0,1,1)
	display.hline(pos_x+2,pos_y+1,3,1)
	display.hline(pos_x+1,pos_y+2,5,1)
	display.hline(pos_x+0,pos_y+3,7,1)
	display.hline(pos_x+2,pos_y+4,3,1)
	display.hline(pos_x+2,pos_y+5,3,1)
	if(disp == 1):
		display.show()
	
def down_arrow(pos_x, pos_y, disp):
	display.hline(pos_x+2,pos_y+0,3,1)
	display.hline(pos_x+2,pos_y+1,3,1)
	display.hline(pos_x+0,pos_y+2,7,1)
	display.hline(pos_x+1,pos_y+3,5,1)
	display.hline(pos_x+2,pos_y+4,3,1)
	display.hline(pos_x+3,pos_y+5,1,1)
	if(disp == 1):
		display.show()
	
def circle(pos_x, pos_y, disp):
	display.hline(pos_x+2,pos_y+0,3,1)
	display.hline(pos_x+1,pos_y+1,5,1)
	display.hline(pos_x+0,pos_y+2,7,1)
	display.hline(pos_x+0,pos_y+3,7,1)
	display.hline(pos_x+1,pos_y+4,5,1)
	display.hline(pos_x+2,pos_y+5,3,1)
	if(disp == 1):
		display.show()
	
def running_avg(trigger_level):
	i = 0
	cur = 0
	trig = 1
	prev_c = sensor.readI()
	peak_c = 0
	prev_v = sensor.readV()/1000
	min_v = 3.3
	avg = 0
	mqtt_try = 1
	#trigger_level = 10
	run_time = time.ticks_ms()
	run_time_trig = 0
	time_avg = 0
	time_avg_count = 0
	cur_avg_total = 0
	cur_avg_total_count = 0
	while a_button.value() == 1:
		if (b_button.value() == 0) or ((sensor.readI() < trigger_level) and (trig == 0)):
			i = 0
			cur = 0
			trig = 1
			time_run = time.ticks_ms()-run_time
			time_avg += time_run
			time_avg_count += 1
			cur_avg_total += avg
			cur_avg_total_count += 1
			
			#print("I_p: ", end =""), print(peak_c, end =" I_avg: "), print(avg, end =" V_min: "), print(min_v, end =" S: "), print(time.monotonic()-run_time, end=""), print("          ")
			print('I_p: {:6.2f}mA  I_avg: {:6.2f}mA  I_avg_total: {:6.2f}mA  V_min: {:3.2f}V  Time: {:3.2f}s  Time_avg: {:3.2f}s     '.format(peak_c, avg, cur_avg_total/cur_avg_total_count, min_v, time_run/1000, time_avg/time_avg_count/1000))
			print('---------------')
			if(mqtt_try == 1):
				try:
					mqtt_send(server, cur_avg_total/cur_avg_total_count, time_avg/time_avg_count/1000, peak_c, min_v, time_avg_count)
				except:
					print('MQTT could not connect')
					print('Won\'t try MQTT again until restart')
					mqtt_try = 0
			avg = 0
			peak_c = 0
			min_v = 3.3
			prev_c = 0
			run_time_trig = 0
			
			oled_clear()
			display.text('I_avg: {:6.2f}mA'.format(cur_avg_total/cur_avg_total_count), 0, 0, 1)
			display.text('T_avg: {:6.2f}s'.format(time_avg/time_avg_count/1000), 0, 10, 1)
			display.text("Count: {:6}".format(time_avg_count), 0, 20, 1)
			display.show()
			#clear()
		if(sensor.readI() > trigger_level):
			if(run_time_trig == 0):
				run_time = time.ticks_ms()
				run_time_trig = 1
			i += 1
			cur += sensor.readI()
			prev_c = sensor.readI()
			prev_v = sensor.readV()/1000
			if(prev_c > peak_c):
				peak_c = prev_c
			if(prev_v < min_v):
				min_v = prev_v				
			avg = cur/i
			trig = 0
			#print("I: ", end =""), print(prev_c, end =" I_p: "), print(peak_c, end =" I_avg: "), print(avg, end =" V: "), print(prev_v, end=" S: "), print(time.monotonic()-run_time, end ="\r")
			print('I: {:6.2f}mA  I_p: {:6.2f}mA  I_avg: {:6.2f}mA  Time: {:3.2f}S      '.format(prev_c, peak_c, avg, (time.ticks_ms()-run_time)/1000), end="\r")
		time.sleep(0.0001)

def running_avg_nomqtt(trigger_level):
	i = 0
	cur = 0
	trig = 1
	prev_c = sensor.readI()
	peak_c = 0
	prev_v = sensor.readV()/1000
	min_v = 3.3
	avg = 0
	#trigger_level = 10
	run_time = time.ticks_ms()
	run_time_trig = 0
	time_avg = 0
	time_avg_count = 0
	cur_avg_total = 0
	cur_avg_total_count = 0
	while a_button.value() == 1:
		if (b_button.value() == 0) or ((sensor.readI() < trigger_level) and (trig == 0)):
			i = 0
			cur = 0
			trig = 1
			time_run = time.ticks_ms()-run_time
			time_avg += time_run
			time_avg_count += 1
			cur_avg_total += avg
			cur_avg_total_count += 1
			
			#print("I_p: ", end =""), print(peak_c, end =" I_avg: "), print(avg, end =" V_min: "), print(min_v, end =" S: "), print(time.monotonic()-run_time, end=""), print("          ")
			print('I_p: {:6.2f}mA  I_avg: {:6.2f}mA  I_avg_total: {:6.2f}mA  V_min: {:3.2f}V  Time: {:3.2f}s  Time_avg: {:3.2f}s     '.format(peak_c, avg, cur_avg_total/cur_avg_total_count, min_v, time_run/1000, time_avg/time_avg_count/1000))
			print('---------------')
			#mqtt_send(server, cur_avg_total/cur_avg_total_count, time_avg/time_avg_count/1000, peak_c, min_v, time_avg_count)
			avg = 0
			peak_c = 0
			min_v = 3.3
			prev_c = 0
			run_time_trig = 0
			
			oled_clear()
			display.text('I_avg: {:6.2f}mA'.format(cur_avg_total/cur_avg_total_count), 0, 0, 1)
			display.text('T_avg: {:6.2f}s'.format(time_avg/time_avg_count/1000), 0, 10, 1)
			display.text("Count: {:6}".format(time_avg_count), 0, 20, 1)
			display.show()
			#clear()
		if(sensor.readI() > trigger_level):
			if(run_time_trig == 0):
				run_time = time.ticks_ms()
				run_time_trig = 1
			i += 1
			cur += sensor.readI()
			prev_c = sensor.readI()
			prev_v = sensor.readV()/1000
			if(prev_c > peak_c):
				peak_c = prev_c
			if(prev_v < min_v):
				min_v = prev_v				
			avg = cur/i
			trig = 0
			#print("I: ", end =""), print(prev_c, end =" I_p: "), print(peak_c, end =" I_avg: "), print(avg, end =" V: "), print(prev_v, end=" S: "), print(time.monotonic()-run_time, end ="\r")
			print('I: {:6.2f}mA  I_p: {:6.2f}mA  I_avg: {:6.2f}mA  Time: {:3.2f}S      '.format(prev_c, peak_c, avg, (time.ticks_ms()-run_time)/1000), end="\r")
		time.sleep(0.0001)
		
def oled_clear():
	display.fill(0)
	display.show()
	
def oled_clear_part():
	display.fill_rect(9,0,119,32,0)
	display.show()

def oled_fill():
	display.fill(1)
	display.show()
	
def oled_fill_menu():
	display.fill_rect(9,0,119,32,1)
	display.show()

def oled_hello():
	display.fill(0)
	display.text('Hello', 0, 0, 1)
	display.text('World', 0, 10, 1)
	display.show()
	
def oled_hello_menu():
	display.fill_rect(9,0,119,32,0)
	display.text('Hello', 10, 0, 1)
	display.text('World', 10, 10, 1)
	display.show()
	
def oled_menu_current(sel_func):
	value = 0
	oled_clear()
	up_arrow(0,0,0)
	down_arrow(0,13,0)
	circle(0,26,0)
	#display.fill_rect(20,13,100,8,0)
	display.text('Menu', 20, 13, 1)
	display.show()
	while True:
		if(b_button.value() == 0):
			value += 1
			display.fill_rect(9,0,119,32,0)
			display.text(str(value), 20, 13, 1)
			display.show()
			time.sleep(0.2)
		if(a_button.value() == 0):
			value -= 1
			if(value < 0):
				value = 0
			#display.fill_rect(20,13,100,8,0)
			display.fill_rect(9,0,119,32,0)
			display.text(str(value), 20, 13, 1)
			display.show()
			time.sleep(0.2)
		if(c_button.value() == 0):
			#display.fill_rect(20,13,100,8,0)
			#display.show()
			sel_func(value)
			break
			time.sleep(0.2)

def oled_menu():
	while True:
		global selection
		#Define Menu Items
		menu_items = ['AVG I NOMQTT', 'Clear', 'Hello World', 'Fill', 'Exit']
		#Functions related to each menu item
		functions = [oled_menu_current,oled_clear_part,oled_hello_menu,oled_fill_menu, 'exit']
		#Arguments to pass if needed
		#Example - passing the function name to be run into the current trigger selection with running_avg_nomqtt
		pass_func = {'AVG I NOMQTT':running_avg_nomqtt, 'Clear':None, 'Hello World':None, 'Fill':None, 'Exit':None}
		if(selection == len(menu_items)-1):
			oled_clear
			selection = 0
			break
		selection = 0
		value = None
		oled_clear()
		up_arrow(0,0,0)
		down_arrow(0,13,0)
		circle(0,26,0)
		#display.fill_rect(9,0,118,32,0)
		display.text('Menu', 20, 13, 1)
		display.show()
		while True:
			if(b_button.value() == 0):
				selection += 1
				if(selection >= len(menu_items)):
					selection = 0
				#display.fill_rect(20,13,100,8,0)
				display.fill_rect(9,0,119,32,0)
				display.text(menu_items[selection], 20, 13, 1)
				display.show()
				value = pass_func[menu_items[selection]]
				time.sleep(0.2)
			if(a_button.value() == 0):
				selection -= 1
				if(selection < 0):
					selection = len(menu_items)-1
				#display.fill_rect(20,13,100,8,0)
				display.fill_rect(9,0,119,32,0)
				display.text(menu_items[selection], 20, 13, 1)
				display.show()
				value = pass_func[menu_items[selection]]
				time.sleep(0.2)
			if(c_button.value() == 0):
				#display.fill_rect(20,13,100,8,0)
				#display.show()
				if(selection == len(menu_items)-1):
					oled_clear()
					break
				if(value == None):
					functions[selection]()
					break
				else:
					functions[selection](value)
					break
				time.sleep(0.2)
	
if __name__ == "__main__":
	print("Ready To Go!")
	oled_menu()
	#running_avg(10)