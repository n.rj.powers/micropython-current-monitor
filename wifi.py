import network
import time

def wifi_connect():
	# Connect to the wifi. Based on the example in the micropython
	# documentation.
	wlan = network.WLAN(network.STA_IF)
	wlan.active(True)
	time.sleep(2)
	if not wlan.isconnected():
		essid = input("SSID: ")
		password = input("PASS: ")
		print('connecting to network ' + essid + '...')
		wlan.connect(essid, password)
		# connect() appears to be async - waiting for it to complete
		while not wlan.isconnected():
			pass
		print('Wifi connect successful, network config: %s' % repr(wlan.ifconfig()))
	else:
		# Note that connection info is stored in non-volatile memory. If
		# you are connected to the wrong network, do an explicity disconnect()
		# and then reconnect.
		print('Wifi already connected, network config: %s' % repr(wlan.ifconfig()))

def wifi_disconnect():
    # Disconnect from the current network. You may have to
    # do this explicitly if you switch networks, as the params are stored
    # in non-volatile memory.
    wlan = network.WLAN(network.STA_IF)
    if wlan.isconnected():
        print("Disconnecting...")
        wlan.disconnect()
    else:
        print("Wifi not connected.")

def disable_wifi_ap():
    # Disable the built-in access point.
    wlan = network.WLAN(network.AP_IF)
    wlan.active(False)
    print('Disabled access point, network status is %s' %
          wlan.status())

def wifi_info():
	wlan = network.WLAN(network.AP_IF)
	return wlan.ifconfig()