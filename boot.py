# This file is executed on every boot (including wake-boot from deepsleep)

#import esp
#esp.osdebug(None)

import gc
import machine
import time

#import webrepl
#webrepl.start()

import time
from machine import I2C, Pin
i2c = I2C(scl = Pin(5), sda = Pin(4), freq=100000)

import ssd1306
display = ssd1306.SSD1306_I2C(128, 32, i2c)

global a_button
global b_button
global c_button
a_button = machine.Pin(0, machine.Pin.IN, machine.Pin.PULL_UP)
b_button = machine.Pin(16, machine.Pin.IN)
c_button = machine.Pin(2, machine.Pin.IN, machine.Pin.PULL_UP)

import network
sta_if = network.WLAN(network.STA_IF)

def connect():
	global sta_if
	sta_if.active(1)
	sta_if.connect()
	time.sleep(1)
	if not sta_if.isconnected():
		SSID = input("SSID: ")
		PASS = input("PASS: ")
		print('connecting to network...')
		sta_if.active(True)
		sta_if.connect(SSID, PASS)
		while not sta_if.isconnected():
			pass
	print('network config:', sta_if.ifconfig())

def no_debug():
    import esp
    # you can run this from the REPL as well
    esp.osdebug(None)
	
def menu():
	display.fill(0)
	display.text('WiFi', 0, 0, 1)
	display.text('A: Enabled', 0, 10, 1)
	display.text('B: Disabled', 0, 20, 1)
	display.show()
	
	while True:
		if(a_button.value() == 0):
			connect()
			break
		if(b_button.value() == 0):
			global sta_if
			sta_if.active(0)
			break

			
# Run Code
reset_reason = machine.reset_cause()

no_debug()
print('Reset reason: ', reset_reason)
if(reset_reason != 4):
	menu()
gc.collect()
#connect()